package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import model.Album;

/**
 * The controller class for the CreateAlbumController view. Allows the 
 * User to create a new Album with a provided name as a copy of the 
 * currently selected Album (used both to create an Album from the results
 * of a search and to create a copy of the currently selected Album).
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class CreateAlbumController extends PhotosController {

	/** The TextField where the user must provide a name for the new Album they are creating.*/
	@FXML
	private TextField newAlbumNameTextField;

	/** The Button that triggers creating a new Album with the name provided in the 
	 * newAlbumNameTextField.
	 */
	@FXML
	private Button createNewAlbumButton;

	/** The Button that cancels creating the Album, returning the User to the PhotoManagement view /
	 * PhotoManagementController. */
	@FXML
	private Button cancelButton;

	@Override
	public String fxmlPath() { 
		return "../view/CreateAlbum.fxml";	
	}

	@Override
	public String viewTitle() {
		return "Create Album";
	}
	
	/**
	 * Actually creates the new Album with the albumName String provided in the 
	 * newAlbumNameTextField. This will fail if either of the following is true:<br> 
	 * -No text has been entered in the newAlbumNameTextField<br>
	 * -An Album with the name provided in the newAlbumNameTextField already exists for the
	 * logged in User 
	 */
	public void createAlbumAction() {
		if(!this.newAlbumNameTextField.getText().isEmpty()) {
			Album tempAlbum = new Album(Photos.transitionController.selectedAlbum, this.newAlbumNameTextField.getText());

			if (Photos.transitionController.loggedInUser.addAlbum(tempAlbum)) {
				Photos.transitionController.selectedAlbum = tempAlbum;
				Photos.transitionController.transitionFromCreateAlbumToPhotoManagement();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Creating Album!");
				alert.setHeaderText("An Album with this name already exists for the User!");
				alert.setContentText("This Album already exists with this name. Please change the provided name and try again.");
				alert.showAndWait();
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Creating Album!");
			alert.setHeaderText("Album name must not be blank!");
			alert.setContentText("An Album name is required to create an Album. Please enter one and try again.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Backs out of the CreateAlbum view / CreateAlbumController, 
	 * returning the User to the PhotoManagement view / PhotoManagementController.
	 */
	public void cancelAction() {
		Photos.transitionController.transitionFromCreateAlbumToPhotoManagement();
	}
}
