package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import model.Tag;

/**
 * The controller class for the ConstructTagSearch view. Allows the 
 * User to select one or two Tag types and enter values for those types 
 * to use to filter their Photos by and to trigger a Tag-based 
 * search using those Tag values and Tag types.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class ConstructTagSearchController extends PhotosController {

	/** The TextField where the user can enter the first of up to two Tag values to search for*/
	@FXML
	private TextField firstTagValueTextField;

	/** The TextField where the user can enter the second of up to two Tag values to search for*/
	@FXML
	private TextField secondTagValueTextField;
	
	/** The ComboBox where the user can select the first of up to two Tag types to search for*/
	@FXML
	private ComboBox<String> firstTagTypeComboBox;
	
	/** The ComboBox where the user can select the second of up to two Tag types to search for*/
	@FXML
	private ComboBox<String> secondTagTypeComboBox;
	
	/** The ToggleGroup used to group the andRadioButton and orRadioButton so that toggling one on
	 * toggles the other off*/
	@FXML
	private ToggleGroup andOrToggleGroup;
	
	/** The RadioButton that corresponds to a "both Tags search". When toggled on, Tag search will
	 * only return Photos that are tagged with both provided Tags.*/
	@FXML
	private RadioButton andRadioButton;

	/** The RadioButton that corresponds to a "either Tag search". When toggled on, Tag search will
	 * return Photos that are tagged with either of the provided Tags.*/
	@FXML
	private RadioButton orRadioButton;
	
	/** The Button that triggers the search. */
	@FXML
	private Button searchButton;
	
	/** The Button that cancels the search, returning the User to the AlbumManagement view /
	 * AlbumManagementController. */
	@FXML
	private Button cancelButton;
	
	@Override
	public String fxmlPath() {
		return "../view/ConstructTagSearch.fxml";
	}

	@Override
	public String viewTitle() {
		return "Construct Tag Search";
	}
	
	/**
	 * On load, add all Tag types to the firstTagTypeComboBox and the String 'None' along with
	 * all Tag types to the secondTagTypeComboBox and toggles the andRadioButton on to default
	 * to the "both search terms" search type.
	 */
	@FXML
	public void initialize() {
		ObservableList<String> firstTagList = FXCollections.observableArrayList();
		ObservableList<String> secondTagList = FXCollections.observableArrayList();
		firstTagList.add(Tag.locationTagString);
		firstTagList.add(Tag.personTagString);
		
		secondTagList.add("None");
		secondTagList.add(Tag.locationTagString);
		secondTagList.add(Tag.personTagString);
		
		firstTagTypeComboBox.setItems(firstTagList);
		firstTagTypeComboBox.getSelectionModel().select(Tag.locationTagString);
		
		secondTagTypeComboBox.setItems(secondTagList);
		secondTagTypeComboBox.getSelectionModel().select("None");
		
		andRadioButton.setSelected(true);
	}
	
	/**
	 * Attempts a Tag search using the one or two Tags the user provides. If the search
	 * encounters multiple copies of the same Photo, it will only return the first copy it encounters.
	 * Searching will fail if any of the folllowing is true:<br>
	 * -the firstTagValueTextField is empty<br>
	 * -the secondTagValueTextField is not empty but 'None' is selected in the secondTagTypeComboBox<br>
	 * -both the firstTagValueTextField and secondTagValueTextFields have values but both the firstTagTypeComboBox
	 * and the secondTagTypeComboBox have the 'Location' Tag type selected. This will never return any results, and so,
	 * is invalid.
	 */
	public void searchAction() {
		if(!secondTagValueTextField.getText().isEmpty() && 
			secondTagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase("None")) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Attempting Search!");
			alert.setHeaderText("Second Tag type is not selected!");
			alert.setContentText("Please select a Second Tag type or remove the text from the Second Search Term text field and try again.");
			alert.showAndWait();
			return;
		}
		
		if(firstTagValueTextField.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Attempting Search!");
			alert.setHeaderText("At least one Tag is required to perform search!");
			alert.setContentText("Please provide a Tag value in the first search term text field and try again.");
			alert.showAndWait();
			return;
		}
		
		if(firstTagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase(Tag.locationTagString) && 
				secondTagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase(Tag.locationTagString) &&
				andRadioButton.isSelected()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Attempting Search!");
			alert.setHeaderText("Cannot search for Photos matching both of two Location tags!");
			alert.setContentText("A Photo can only be tagged with one location Tag, searching for Photos matching both of two location Tags will never always return 0 photos. Please address this and try again.");
			alert.showAndWait();
			
			return;
		}
		
		Tag firstTag = null;
		Tag secondTag = null;
		
		firstTag = new Tag(firstTagTypeComboBox.getSelectionModel().getSelectedItem(), firstTagValueTextField.getText()); 
		
		if(!secondTagValueTextField.getText().isEmpty() && !secondTagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase("None")) {
			secondTag = new Tag(secondTagTypeComboBox.getSelectionModel().getSelectedItem(), secondTagValueTextField.getText());
		}
		
		if(andRadioButton.isSelected()) {
			Photos.transitionController.selectedAlbum = Photos.transitionController.loggedInUser.createAlbumFromBothTagsSearchCriteria(firstTag, secondTag);
		} else {
			Photos.transitionController.selectedAlbum = Photos.transitionController.loggedInUser.createAlbumFromEitherTagsSearchCriteria(firstTag, secondTag);
		}
		
		Photos.transitionController.transitionFromTagSearchToPhotoManagement();
	}
	
	/**
	 * Backs out of the ConstructTagSearch view / ConstructTagSearchController, 
	 * returning the User to the AlbumManagement view / AlbumManagementController.
	 */
	public void cancelAction() {
		Photos.transitionController.transitionFromTagSearchToAlbumManagement();
	}
}
