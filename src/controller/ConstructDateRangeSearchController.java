package controller;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

/**
 * The controller class for the ConstructDateRangeSearch view. Allows the 
 * User to enter one or two Dates to use to filter their Photos by and
 * to trigger a Date range search using those Dates.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class ConstructDateRangeSearchController extends PhotosController {

	/** The Button that triggers the search. */
	@FXML
	private Button searchButton;
	
	/** The Button that cancels the search, returning the User to the AlbumManagement view /
	 * AlbumManagementController. */
	@FXML
	private Button cancelButton;
	
	/** The TextField where the User may provide a start Date (inclusive) to filter their
	 * Photos by. This Date must be entered in MM/dd/yyyy format.*/
	@FXML
	private TextField startDateTextField;

	/** The TextField where the User may provide an end Date (inclusive) to filter their
	 * Photos by. This Date must be entered in MM/dd/yyyy format.*/
	@FXML
	private TextField endDateTextField;
	
	@Override
	public String fxmlPath() { 
		return "../view/ConstructDateRangeSearch.fxml";
	}

	@Override
	public String viewTitle() {
		return "Construct Date Range Search";
	}
	
	/**
	 * Validates that the provided String is in the MM/dd/yyyy format
	 * required by the program and if it is, parses the String to a 
	 * Date.
	 * 
	 * @param textToValidate The String value to attempt to parse into a Date object.
	 * @return The parsed Date object corresponding to the provided String or null if 
	 * either the provided String is empty or is in an invalid format. 
	 */
	public Date validateDateText(String textToValidate) {
		Date returnDate;
		
		if(textToValidate == null) {
			return null;
		}
		
		SimpleDateFormat validFormat = new SimpleDateFormat("MM/dd/yyyy");
		
		try {
			validFormat.setLenient(false);
			returnDate = validFormat.parse(textToValidate);
		} catch (ParseException e) {
			return null;
		}
		
		return returnDate;
	}
	
	/**
	 * Attempts a Date range search using the one or two Dates the user provides. If the search
	 * encounters multiple copies of the same Photo, it will only return the first copy it encounters.
	 * Searching will fail if any of the folllowing is true:<br>
	 * -both the provided startDate and the provided endDate are null<br>
	 * -the provided endDate is not after the provided startDate<br>
	 * -either provided Date is invalid. 
	 */
	public void searchAction() {
		Date startDate = validateDateText(startDateTextField.getText());
		Date endDate = validateDateText(endDateTextField.getText());
		
		if(startDate == null && endDate == null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Attempting Search!");
			alert.setHeaderText("At least one cutoff Date is required to search!");
			alert.setContentText("Please provide at least one valid cutoff Date and try again. Dates must be in MM/dd/yyyy format");
			alert.showAndWait();
		} else {
			if(startDate != null && endDate != null && !startDate.before(endDate)) { 
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Attempting Search!");
				alert.setHeaderText("End Date cutoff must be after start Date cutoff!");
				alert.setContentText("Please provide an end Date that occured after the provided start Date and try again.");
				alert.showAndWait();
			} else if (startDate != null && endDate == null && !endDateTextField.getText().isEmpty() ||
					startDate == null && endDate != null && !startDateTextField.getText().isEmpty()) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Attempting Search!");
				alert.setHeaderText("One of the provided cutoff Dates is invalid!");
				alert.setContentText("Please confirm both of your cutoff dates are in MM/dd/yyyy format or remove the invalid Date and try again.");
				alert.showAndWait();
			} else {
				Photos.transitionController.selectedAlbum = Photos.transitionController.loggedInUser.createAlbumFromDateRangeSearchCriteria(startDate, endDate);
				Photos.transitionController.transitionFromDateRangeSearchToPhotoManagement();				
			}
		}
	}
	
	/**
	 * Backs out of the ConstructDateRangeSearch view / ConstructDateRangeSearchController, 
	 * returning the User to the AlbumManagement view / AlbumManagementController.
	 */
	public void cancelAction() {
		Photos.transitionController.transitionFromDateRangeSearchToAlbumManagement();
	}
}
