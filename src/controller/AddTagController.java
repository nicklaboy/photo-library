package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import model.Tag;

/**
 * The controller for the AddTag view which asks the User to add a Tag to the selected Photo.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class AddTagController extends PhotosController {
	
	/** The TextField where the user will enter the value of the Tag to be added.*/
	@FXML
	private TextField tagValueTextField;

	/** The Button that triggers adding the new Tag to the selected Photo (assuming that tag is valid)
	 * and returning to the PhotoManagement view / PhotoManagementController.*/
	@FXML
	private Button addTagButton;

	/** The Button that allows the User to back out of the AddTag view / AddTagController,
	 * returning them to the PhotoManagement view / PhotoManagementController.*/
	@FXML
	private Button cancelButton;
	
	/** The ComboBox that lists the available Tag types that can be added to the Photo.*/
	@FXML
	private ComboBox<String> tagTypeComboBox;
	
	@Override
	public String fxmlPath() { 
		return "../view/AddTag.fxml";	
	}

	@Override
	public String viewTitle() {
		return "Add Tag To Photo";
	}
	
	/**
	 * Populate the tagTypeComboBox and select its first item on load.
	 */
	@FXML
	public void initialize() {
		tagTypeComboBox.getItems().add(Tag.personTagString);
		if(!Photos.transitionController.selectedPhoto.hasLocationTag()) {
			tagTypeComboBox.getItems().add(Tag.locationTagString);			
		}
		
		tagTypeComboBox.getSelectionModel().select(0);
	}
	
	/**
	 * Determines whether there is a valid Tag to be added using the following
	 * validation:<br>
	 * -the tagValueTextField must actually contain text<br>
	 * -if the User is trying to add a new person tag, that person must not already be tagged for this Photo<br>
	 * -if the User is trying to add a new location tag, this Photo must not already have a location tag<br><br>
	 * If the Tag passes all these tests,
	 * a new Tag object is created and added to the current Photo for the current Album and User.
	 */
	public void addTagAction() {
		if(this.tagValueTextField.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding Tag!");
			alert.setHeaderText("No tag value entered!");
			alert.setContentText("Tag value cannot be empty.");
			alert.showAndWait();
			return;
		}
		
		if(this.tagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase(Tag.personTagString) && 
				Photos.transitionController.selectedPhoto.personIsAlreadyTagged(this.tagValueTextField.getText())) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding Tag!");
			alert.setHeaderText("This person is already tagged!");
			alert.setContentText("The same person cannot be tagged multiple times in the same photo.");
			alert.showAndWait();
			
			return;
		}
		
		if(this.tagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase(Tag.locationTagString) &&
				Photos.transitionController.selectedPhoto.hasLocationTag()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding Tag!");
			alert.setHeaderText("This photo already has a location tag!");
			alert.setContentText("A location may not be tagged with more than one location.");
			alert.showAndWait();
			return;
		}		
		
		if(tagTypeComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase(Tag.locationTagString)) {
			Photos.transitionController.selectedPhoto.addLocationTag(tagValueTextField.getText());
		} else {
			Photos.transitionController.selectedPhoto.addPersonTag(tagValueTextField.getText());
		}
		Photos.transitionController.transitionFromAddTagToPhotoManagement();		
	}
	
	/** Back out of the AddTag view / AddTagController,
	 * returning them to the PhotoManagement view / PhotoManagementController.*/
	public void cancelAction() {
		Photos.transitionController.transitionFromAddTagToPhotoManagement();
	}
}
