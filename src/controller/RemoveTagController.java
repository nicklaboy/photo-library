package controller;

import java.util.Optional;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import model.Tag;

/**
 * The controller class for the RemoveTag view. Allows the 
 * User to select and remove a Tag from the selected Photo. 
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class RemoveTagController extends PhotosController {

	/** The ListView containing all of the Tags the current Photo is tagged with.*/
	@FXML
	private ListView<Tag> currentTagsListView;
	
	/** The Button that triggers removing the selected Tag from the selected Photo 
	 * and returning to the PhotoManagement view / PhotoManagementController.*/
	@FXML
	private Button removeSelectedTagButton;
	
	/** The Button that allows the User to back out of the RemoveTag view / RemoveTagController,
	 * returning them to the PhotoManagement view / PhotoManagementController.*/
	@FXML
	private Button cancelButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/RemoveTag.fxml";
	}

	@Override
	public String viewTitle() {
		return "Remove Tag From Photo";
	}
	
	/**
	 * On load, populate the currentTagsListView and define the way it builds its cells.
	 */
	public void initialize() {
		currentTagsListView.setItems(Photos.transitionController.selectedPhoto.getAllTags());
		
		currentTagsListView.setCellFactory(param -> new ListCell<Tag>() {
            @Override
            public void updateItem(Tag tag, boolean empty) {
                super.updateItem(tag, empty);
                if (empty) {
                    setText(null);
                } else {
                	setText(tag.fullToString());
                }
            }
        });
	}
	
	/**
	 * Determines whether a Tag was selected and if one was, removes it from the selected Photo.
	 */
	public void removeTagAction() {
		if(currentTagsListView.getSelectionModel().isEmpty() || currentTagsListView.getItems().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Removing Tag!");
			alert.setHeaderText("No Tag selected!");
			alert.setContentText("Please select a Tag and try again.");
			alert.showAndWait();
		} else {
			if(Photos.transitionController.selectedPhoto != null) {
				Alert confirmation = new Alert(AlertType.CONFIRMATION);
				confirmation.initOwner(mainStage); 
				confirmation.setTitle("Remove Tag From Photo");
				confirmation.setHeaderText("Are you sure you want to remove this Tag?");
				Optional<ButtonType> result = confirmation.showAndWait();

				if (result.get() == ButtonType.OK){
					Photos.transitionController.selectedPhoto.removeTag(currentTagsListView.getSelectionModel().getSelectedItem());
					Photos.transitionController.transitionFromRemoveTagToPhotoManagement();
				}
			}
		}
	}
	
	/** Back out of the RemoveTag view / RemoveTagController,
	 * returning them to the PhotoManagement view / PhotoManagementController.*/
	public void cancelAction() {
		Photos.transitionController.transitionFromRemoveTagToPhotoManagement();
	}
}
