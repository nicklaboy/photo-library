package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;

/**
 * The controller for the custom prompt which asks the User to add a Photo to the selected Album.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 *
 */
public class AddPhotoController extends PhotosController {
	/** The TextField that will populate with the path and filename 
	 * for the picture the user wishes to add. Automatically populates with
	 * the path and filename of a file the user selects.*/
	@FXML
	private TextField filenameTextField;
	
	/** The Button that triggers the FileChooser allowing the user to add a photo
	 * from their system.*/
	@FXML
	private Button filePickerButton;
	
	/** The Button that triggers actually adding the Photo to the current User's 
	 * selected Album. Once added, we return to the PhotoManagement view / 
	 * PhotoManagementController.*/
	@FXML
	private Button addPhotoButton;
	
	/** The Button that allows the User to back out of the AddPhoto view / AddPhotoController,
	 * returning them to the PhotoManagement view / PhotoManagementController.*/
	@FXML
	private Button cancelButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/AddPhoto.fxml";	
	}

	@Override
	public String viewTitle() {
		return "Add Photo";
	}
	
	/**
	 * Displays up a FileChooser for the user and if the user picks
	 * an existing file from that FileChooser, populates the 
	 * filenameTextField with that file's path and name.
	 */
	public void filePickerAction() {
		FileChooser newFilePicker = new FileChooser();
		newFilePicker.setTitle("Locate image...");
		File file = newFilePicker.showOpenDialog(mainStage);
		if(file != null && file.exists()) {
			this.filenameTextField.setText(file.getAbsolutePath());
		}
	}
	
	/**
	 * Determines whether there is a valid Photo to be added using the following
	 * validation:<br>
	 * -the filenameTextField must actually contain text<br>
	 * -that text must correspond to an existing file in the file system<br>
	 * -that file must be an image<br>
	 * -that image must not already be a part of the currently selected Album
	 * for the currently selected User.<br><br>
	 * If the image passes all these tests,
	 * a new Photo object is created and added to the current Album for the current User.
	 */
	public void addPhotoAction() {
		if(this.filenameTextField.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding File!");
			alert.setHeaderText("Invalid File Path!");
			alert.setContentText("File path cannot be empty.");
			alert.showAndWait();
			
			return;
		}
		
		File file = new File(this.filenameTextField.getText());
		if(file.exists()) {
			
			boolean valid = true;
			try {
			    BufferedImage image = ImageIO.read(file);
			    if (image == null) {
			        valid = false;
			        
			    }
			} catch(IOException ex) {
			    valid = false;
			}
			
	        if(valid) {
				if(!Photos.transitionController.selectedAlbum.addPhoto(this.filenameTextField.getText())) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.initOwner(mainStage); 
					alert.setTitle("Error Adding File!");
					alert.setHeaderText("Invalid File Path!");
					alert.setContentText("Please confirm that your file exists and isn't already part of this album and try again. \n\nIf you continue to experience issues, contact your administrator.");
					alert.showAndWait();
				} else {
		        	Photos.transitionController.transitionFromAddPhotoToPhotoManagement();					
				}
	        } else {
	        	Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Adding File!");
				alert.setHeaderText("Invalid File Type!");
				alert.setContentText("Please confirm that your file is an image and try again. \n\nIf you continue to experience issues, contact your administrator.");
				alert.showAndWait();
	        }
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding File!");
			alert.setHeaderText("Invalid File Path!");
			alert.setContentText("Please confirm that your file exists and isn't already part of this album and try again. \n\nIf you continue to experience issues, contact your administrator.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Backs out of the AddPhoto view / AddPhotoController, returning the User to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void cancelAction() {
		Photos.transitionController.transitionFromAddPhotoToPhotoManagement();
	}
	
}
